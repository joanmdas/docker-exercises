# Elasticsearch + Kibana compose
This is the resulting docker-compose file of this exercise:
```yaml
version: '3.6'
services:
  elasticsearch:
    image: elasticsearch:7.9.3
    container_name: elasticsearch
    environment:
      - "discovery.type=single-node"
    networks:
      - elastic
    ports:
      - "9200:9200"
      - "9300:9300"
  kibana:
    image: kibana:7.9.3
    container_name: kibana
    networks:
      - elastic
    environment:
      ELASTICSEARCH_HOST: "elasticsearch"
      ELASTICSEARCH_PORT: "9200"
    ports:
      - "5601:5601"
    depends_on:
      - "elasticsearch"

networks:
  elastic:
```
The file was executed with the command ```docker-compose up -d```. To stop and remove the containers and network created, 
the command ```docker-compose down``` was executed.

This resulted in the download of the aforementioned images in the "image:" for elasticsearch and kibana, the creation of 
its containers using the defined ports, environment variables, etc. Also, the creation of the "elastic" bridge network.

![Alt](./resources/md_images/5-network-ls.png "Network")

![Alt](./resources/md_images/5-container-ls.png "Container")

The resulting website found in the URL 'http://localhost:5601':
![Alt](./resources/md_images/5-try-sample-data.png "Try sample data")

![Alt](./resources/md_images/5-add-sample-data.png "Add sample data")

![Alt](./resources/md_images/5-dashboard.png "Dashboard")