# Health check

According to this exercise, the following Dockerfile was created:
```dockerfile
FROM nginx:1.19.3-alpine
COPY ./static_content /usr/share/nginx/html
EXPOSE 8080
HEALTHCHECK --interval=45s --timeout=5s --start-period=5s --retries=2 \
CMD curl -f http://localhost:80 || exit 1
```
Every 45 seconds we will execute the command defined in the CMD to check for the health. If there is no response in 
5 seconds defined in the timeout, it will be considered as a failure. The health checks done before 5 seconds after 
starting the container will be ignored if they fail since the container might be starting. If one check is successful 
inside the start-period, the container will be considered as started and all the consecutive failures will be counted 
towards the maximum number of retries. If 2 consecutive retries occur with failure, the container will be considered 
unhealthy. 

To test the health, the command stated in CMD will be executed. It will send a request to the internal port to see if it
is available.

To run this Dockerfile, the following commands were executed.
```
docker build -t exercici4:0.0.1 .           #Create image
docker run -d -p 8080:80 exercici4:0.0.1    #Run image
```
The result:
![Alt](./resources/md_images/4-result.png "Result")