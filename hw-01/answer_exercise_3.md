#Working with volumes

We created a simple Dockerfile that downloads the 1.19.3-alpine version of nginx,
```dockerfile
FROM nginx:1.19.3-alpine    #Download nginx version 1.19.3 alpine image
EXPOSE 8080                 #Expose port 8080 to host
```
The executed command is the following
```
docker build -t exercise3:0.0.1 .   #Build the image 
docker run -d 
    -p 8080:80                                                  #Map the host 8080 port to container 80 port
    -v /<absolute-path>/static_content:/usr/share/nginx/html    #Link the host folder to the container folder
    exercise3:0.0.1                                             #<image>:<version> to run
```
When this container is running, we can modify the content of the local index.html stored in the static_content folder
and the changes will be applied without having to restart the container.

![Alt](./resources/md_images/3-result.png "Original page")

![Alt](./resources/md_images/3-result-file-modified.png "Modified page")