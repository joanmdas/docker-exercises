# Difference between ENTRYPOINT and ENV in Dockerfile
ENTRYPOINT defines a command to be executed when the container starts. This command can't be overwritten, only complemented.
There can only be **one ENTRYPOINT** instruction in a Dockerfile. If there is more than one, then the last one will be used.

The ENTRYPOINT command can be specified in the 'shell form', or the 'exec form'. The preferred form is the 'exec form'.
```dockerfile
ENTRYPOINT ["executable","param1","param2"]    #Exec form
ENTRYPOINT command param1 param2               #Shell form
```
CMD is an instruction that specifies which command to execute when a docker container starts. There can only be **one CMD** 
instruction in a Dockerfile. If there is more than one, then the last one will be used. The CMD defines default commands 
and/or parameters for a container which can be overwritten.

The CMD command can also be specified in the 'shell form', or the 'exec form'. The preferred form is the 'exec form'.
```dockerfile
CMD ["executable","param1","param2"]    #Exec form
CMD command param1 param2               #Shell form
```
Examples:
```dockerfile
FROM ubuntu
CMD ["echo", "Hello World"]
```
When running this Dockerfile with the command ```docker run [image_name]```, the default CMD will be executed and it will
print the message "Hello World". However, if you add a parameter when running the image ```docker run [image_name] hostname```,
the result will be the execution of the command ```echo hostname```.
```dockerfile
FROM ubuntu
ENTRYPOINT ["echo", "Hello World"]
```
In this case, when running the ```docker run [image_name]```, it will print the same as with CMD, "Hello World". But when 
we add a parameter when running the image like ```docker run [image_name] Peace```, it will print the message "Hello World Peace".
As it can be seen, the command defined in the ENTRYPOINT was maintained.

There is also the case when you combine ENTRYPOINT and CMD command.
```dockerfile
FROM ubuntu
ENTRYPOINT ["echo", "Hello"]
CMD ["World"]
```
When running this Dockerfile with ```docker run [image_name]```, you obtain the message "Hello World". However, if you 
execute the command ```docker run [image_name] Joan```, the message obtained will be "Hello Joan". That means that the 
command defined in the ENTRYPOINT was maintained, and the parameter defined in the CMD, with the default value "World", 
overwritten.

**CMD and ENTRYPOINT can work alongside the ENV command** in the following way. You have to take into account though that in order to 
do that you have to use the shell form or execute a shell directly since the exec form will not invoke a command shell. 
In this cas, it means that it will not do variable substitution on environment variables.
```dockerfile
ENV NAME="Joan Temprano"
CMD ["echo", "Hello, $NAME"]                    #This will print 'Hello, $NAME'
CMD ["sh", "-c", "echo Hello, $NAME"]           #This will print 'Hello, Joan Temprano'
ENTRYPOINT [ "sh", "-c", "echo Hello, $NAME" ]  #This will print 'Hello, Joan Temprano'
```

EXTRA:

ENV is an instruction that sets an environment variable. Inside a Dockerfile there can be **multiple ENV** commands.
```dockerfile
ENV <key>=<value>
ENV <key2>=<value2>
```
It is also possible to define multiple environment variables using a single ENV command.
```dockerfile
ENV <key>=<value> <key>=<value>
```
If there is a need for the value to have spaces, they can be parsed by using the quotes and backslash.
```dockerfile
ENV NAME="Joan Temprano" SUBJECT=Docker\ exercices
```
These values persist inside the container when it starts.