# Difference between ADD and COPY in Dockerfile

ADD and COPY commands functionality are similar, but ADD command has extra features that COPY does not have. The 
**general rule** is that, if you don't need those extra features of ADD, use the COPY command.

Both COPY and ADD basic functionality is to copy local files into a container, from the <src> to the <dest>.
```dockerfile
COPY [--chown=<user>:<group>] <src>... <dest>
COPY [--chown=<user>:<group>] ["<src>",... "<dest>"]    #This form is required when whitespaces are present in the paths for <src> or <dest>

ADD [--chown=<user>:<group>] <src>... <dest>
ADD [--chown=<user>:<group>] ["<src>",... "<dest>"]     #This form is required when whitespaces are present in the paths for <src> or <dest>
```
The difference is that, while both allow the copy of files and directories from <src> to <dest>, ADD allows you to copy 
a file obtained from a remote URL to <dest>. ADD also supports the automatic extraction of tar files in the <dest>.

As a side note, it is stated in the official documentation that the use of ADD to obtain tar files from remote URL is 
discouraged (it is fine if the compressed file is obtained from the local resource). Instead, it is recommended to use 
'curl' or 'wget' instead so that you can delete the files you no longer need after they’ve been extracted. For example, 
avoid this:
```dockerfile
ADD https://example.com/big.tar.xz /usr/src/things/
RUN tar -xJf /usr/src/things/big.tar.xz -C /usr/src/things \
    && make -C /usr/src/things all
```
And use this instead:
```dockerfile
RUN mkdir -p /usr/src/things \
    && curl -SL https://example.com/big.tar.xz \
    | tar -xJC /usr/src/things \
    && make -C /usr/src/things all
```
In this way we can use a single layer instead of two layers.