This project contains the exercises from "Principios y herramientas de desarrollo" subject.

The project structure is as following:

* hw-01: Exercises responses for hw-01. Delivery date: 10/11/2020
    * resources: folder containing the files used in the exercises.
        * md_images: contains images used in the exercise response.
        * 3: Dockerfile for the 3rd question. 
        * 4: Dockerfile for the 4th question.
        * 5: docker-compose for the 5th question